import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity,
  Image
} from 'react-native';

import api from './services/api';

export default class App extends Component {

  enviarGasto = async () =>{
    const send = await api.post("/expense");
  }



  render(){
    return(
      <View style={st.container}>
        <Image style={styles.foto} source={require('./Imagens/logo.jpg')}/>
        <Text style={styles.logo}>Bem vindo ao iDOG</Text>
        <Text style={styles.pergunta}>Quanto você gastou de ração?</Text>

        <View style={valor.gasto}>
            <Text style={valor.cifao}> R$ </Text>
            <TextInput style={valor.box} placeholder="Seu Gasto Aqui" keyboardType="numeric"/>
        </View>

        <TouchableOpacity style={{width: 100, 
                                  height: 50, 
                                  backgroundColor: '#696969', 
                                  borderRadius: 10, 
                                  marginTop: 20,
                                  justifyContent: 'center',
                                  alignItems:'center'
                                  }}>
          <Text styles={valor.enviar}>Enviar</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const st =StyleSheet.create({
  container: {
    marginTop: 32,
    paddingHorizontal: 24,
    alignItems: "center",
    justifyContent: 'center'
  },
});

const styles = StyleSheet.create({
  foto:{
    marginTop: 150,
    width: 120,
    height: 120,
    
  },

  logo:{
    marginTop: 20,
    fontFamily: 'system-ui',
    fontSize: 30
    
  },
  pergunta:{
    marginTop: 10,
    fontSize: 20
  },
});

const valor = StyleSheet.create({
  gasto:{
    flexDirection: "row",
  },
  cifao:{
    marginTop: 10,
    textAlignVertical: "center",
    fontSize: 30
  },
  box:{
    marginTop: 20,
    borderWidth: 2,
  },
  enviar:{
  }
});